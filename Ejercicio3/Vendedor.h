#include <iostream>
#include "Departamento.h"

using namespace std;

#ifndef VENDEDOR_H
#define VENDEDOR_H

class Vendedor{
    private:
        int num_departamentos;
        int posicion;
        Departamento *departamentos = NULL;

    public:
        Vendedor(int num_departamentos);
        void llenar_arreglo();
        void imprimir_disponible_precio();
        void imprimir_disponible_superficie();
        void valor_arrendados();
        void arrendar_por_eleccion();
        void actualizar_precios();
        void estado_disponibilidad(int posicion);

};
#endif    
