						    
                                                        	INMOBILIARIA ICB          
							   ------------------------

+ Empezando
    El programa cosiste en un simulador de una inmobiliaria, inicialmente presenta un menú con 6 opciones. La primera opción es mostrar deptos disponibles según un precio buscado, se le solicitara el precio máximo que pagaria y se le entregaran los departamentos disponibles. La segunda opción le permite observar que deptos se encuentran disponibles en una ubicación excelente y de la superficei que desea por lo tanto se le pedira que ingrese la extensión de su hogar soñado. En la tercera opción se le mostrara el valor de los departamentos que ya estan arrendados. La cuarta opción puede elegir características personalizadas para la busqueda, se le da la posibilidad de ingresar un precio, la ubicación y la superfiecie. La quinta elección permite modificar el precio de los departamentos disponibles . Finalmente la septima opción le permite al usuario cerrar el programa.

Generalmente todos los departamentos estan arrendados.


+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	[3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./programa parámetro.
		parámetro-> Número de departamentos a crear.
        

+ Construido con
    Sistema operativo: Ubuntu 18.04.3 LTS.
    Vim: Editor de texto para escribir el código del programa. 
    C++: Lenguaje de programación imperativo orientado a objetos derivado del lenguaje C.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

+ Versiones
    Ubuntu 18.04.3 LTS.
    c++ (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0

+ Autor
    Nicole Soto.




