#include <iostream>

using namespace std;

#ifndef DEPARTAMENTO_H
#define DEPARTAMENTO_H

class Departamento{
    private:
        int clave_id;
        int superficie;
        int precio;
        bool disponibilidad;
        string ubicacion;
        
    public:
        Departamento();

        void add_datos();

        void set_disponibilidad(bool disponibilidad);
        void set_precio(int precio);

        int get_clave_id();
        int get_superficie();
        int get_precio();
        bool get_disponibilidad();
        string get_ubicacion();

};
#endif
