#include <iostream>
#include "Departamento.h"
#include "Vendedor.h"

using namespace std;

void menu(Vendedor vendedor){
    int opcion;
    
    cout << "\nMENU\n" << endl;
    cout << " [1] Mostrar deptos disponibles según un precio buscado" << endl;
    cout << " [2] Mostrar deptos de ubición excelente por superficie buscada\n [3] Valor deptos arrendados" << endl;
    cout << " [4] Arrendar por preferencia\n [5] Actualizar precios" << endl;
    cout << " [6] Salir" << endl;
     
    cout << "\n Ingrese su opción: ";
    cin >> opcion;
    
    system("clear");

    while(getchar() != '\n');

    switch(opcion){
        case 1:
            vendedor.imprimir_disponible_precio();
            //system("clear");
            menu(vendedor);
            break;

        case 2:
            vendedor.imprimir_disponible_superficie();
            menu(vendedor);
            break;

        case 3:
            vendedor.valor_arrendados();
            menu(vendedor);
            break;

        case 4:
            vendedor.arrendar_por_eleccion();
            menu(vendedor);
            break;
        
        case 5:
            vendedor.actualizar_precios();
            menu(vendedor);
            break;

        case 6:
            break;

    }

}
 

int main(int argc, char *argv[]){
    int num_departamentos = stoi(argv[1]);
    
    cout << "\n\tINMOBILIARIA ICB\n" << endl;

    Vendedor vendedor = Vendedor(num_departamentos);
    vendedor.llenar_arreglo();
    
    menu(vendedor);
    
    return 0;
}
