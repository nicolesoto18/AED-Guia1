#include <iostream>
#include "Departamento.h"
#include "Vendedor.h"
 
Vendedor::Vendedor(int num_departamentos){
    this->num_departamentos = num_departamentos;
    this->departamentos = new Departamento[this->num_departamentos];
}


void Vendedor::llenar_arreglo(){
    for(int i = 0; i < this->num_departamentos; i++){
        this->departamentos[i].add_datos();
    }
}

// Se mostrara los datos si se encuentra bajo el precio ingresado
void Vendedor::imprimir_disponible_precio(){
    int precio_buscado;

    cout << "¿Hasta qué precio desea buscar?" << endl;
    cout << "--> ";
    cin >> precio_buscado;

    for(int i = 0; i < this->num_departamentos; i++){
        if (this->departamentos[i].get_precio() <= precio_buscado && this->departamentos[i].get_disponibilidad() == true){
            cout << "Esta disponible el departamento: " << this->departamentos[i].get_clave_id() << endl;
            cout << "Precio $" << this->departamentos[i].get_precio() << endl;
            cout << "Superficie: " << this->departamentos[i].get_superficie() << endl;
            cout << "Ubicación: " << this->departamentos[i].get_ubicacion() << endl;
            estado_disponibilidad(i);
            cout << '\n';
        }
            
        else{
            cout << "No hay departamentos disponibles para este precio" << endl;
        }    
    
    }
}



void Vendedor::imprimir_disponible_superficie(){
    int superficie_buscada;

    cout << "¿Cuál es el tamaño mínimo de su hogar deseado?" << endl;
    cout << "--> ";
    cin >> superficie_buscada;

    for(int i = 0; i < this->num_departamentos; i++){
        if (this->departamentos[i].get_superficie() >= superficie_buscada && this->departamentos[i].get_disponibilidad() == true){
            if(this->departamentos[i].get_ubicacion() == "Excelente"){
                cout << "Esta disponible el departamento: " << this->departamentos[i].get_clave_id() << endl;
                cout << "Precio $" << this->departamentos[i].get_precio() << endl;
                cout << "Superficie: " << this->departamentos[i].get_superficie() << endl;
                cout << "Ubicación: " << this->departamentos[i].get_ubicacion() << endl;
                estado_disponibilidad(i);
            }
        }
            
        else{
            cout << "No hay departamentos disponibles para esta superficie" << endl;
        }    
    
    }
}


// Imprime el estado del departamento
void Vendedor::estado_disponibilidad(int posicion){
    if (departamentos[posicion].get_disponibilidad() == true){
        cout << "Disponibilidad: Verdadero" << endl;
    }

    else if(departamentos[posicion].get_disponibilidad() == false){
        cout << "Disponibilidad: Falso" << endl;
    }
}


void Vendedor::valor_arrendados(){
    for(int i = 0; i < this->num_departamentos; i++){
        if (departamentos[i].get_disponibilidad() == false){
            cout << "\n\tDEPARTAMENTOS ARRENDADOS\n" << endl;

            cout << "Clave: " << this->departamentos[i].get_clave_id() << endl;
            cout << "Precio $" << this->departamentos[i].get_precio() << "\n" << endl;
            
        }

        else{
            cout << "Todos los departamentos estan disponibles" << endl;
        }
    }
}

// Se imprimiran los datos si se cumplen los valores ingresados por el ususario
void Vendedor::arrendar_por_eleccion(){
    int superficie_buscada;
    int precio_buscado;
    string ubicacion_buscada;

    cout << "¿Cuál es el tamaño mínimo de su hogar deseado?" << endl;
    cout << "--> ";
    cin >> superficie_buscada;

    cout << "\n¿Cuál es la ubicación que busca?" << endl;
    cout << " - Excelente \n - Buena \n - Regular \n - Mala" << endl;
    cout << "--> ";
    cin >> ubicacion_buscada;

    cout << "\nIngrese el precio que pagara: " << endl;
    cin >> precio_buscado;
    
    while(getchar() != '\n');

    for(int i = 0; i < this->num_departamentos; i++){
        if (this->departamentos[i].get_superficie() >= superficie_buscada && this->departamentos[i].get_disponibilidad() == true){
            if(this->departamentos[i].get_ubicacion() == ubicacion_buscada && this->departamentos[i].get_precio() <= precio_buscado){
                cout << "Se le ha arrendado el departamento: " << this->departamentos[i].get_clave_id() << endl;
                cout << "Precio $" << this->departamentos[i].get_precio() << endl;
                cout << "Superficie: " << this->departamentos[i].get_superficie() << endl;
                cout << "Ubicación: " << this->departamentos[i].get_ubicacion() << endl;
                departamentos[i].set_disponibilidad(false);
            }
        }
            
        else{
            cout << "No hay departamentos disponibles según esas características" << endl;
        }
    } 
}   
        

void Vendedor::actualizar_precios(){
    float porcentaje;
    float aumento;
    float nuevo_precio;

    cout << "¿En qué porcentaje desea aumentar el precio de los departamentos?" << endl;
    cin >> porcentaje;
    
    while(getchar() != '\n');
    
    for(int i = 0; i < this->num_departamentos; i++){
        if (this->departamentos[i].get_disponibilidad() == true){
            // Calcular porcentaje en que aumentara el precio
            aumento = (this->departamentos[i].get_precio() * porcentaje) / 100;
            
            // Se suma el precio antiguo al aumento calculado para el porcentaje
            nuevo_precio = (aumento + this->departamentos[i].get_precio());
            this->departamentos[i].set_precio(nuevo_precio);

            cout << "\n\tACTUALIZACIÓN DE PRECIOS\n" << endl; 
            cout << "El valor de " << this->departamentos[i].get_clave_id() << " ahora es " << departamentos[i].get_precio();
        }

        else{
            cout << "\nNo se actulizaron los precios, no hay departamentos disponibles" << endl;
        }
    }
}


