#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "Departamento.h"

using namespace std;

// Constructor por defecto
Departamento::Departamento(){
}


// Todos los datos se crean de forma aleatoria
void Departamento::add_datos(){
    int random_ubicacion;

    this->clave_id = rand()%101;

    // La superficie ira desde 30 m a 120 m
    this->superficie = 30 + rand()%(121 - 30);
    
    random_ubicacion = rand()%4;

    // Otorga un valor dependiendo del número entregado por el random
    if (random_ubicacion == 0){
        this->ubicacion = "Excelente";
    }
    
    else if(random_ubicacion == 1){
        this->ubicacion = "Buena";
    }
        
    else if(random_ubicacion == 2){
        this->ubicacion = "Regular";
    }

    else if(random_ubicacion == 3){
        this->ubicacion = "Mala";
    }

    // El precio más alto es $100.000
    this->precio = 25000 + rand()%(100000 - 25000);
    
    // Disponibilidad 0 --> Esta arrendado
    // Disponibilidad 1 --> No esta arrendado

    this->disponibilidad = rand()%2;
    
}


int  Departamento::get_clave_id(){
    return this->clave_id;
}


int  Departamento::get_superficie(){
    return this->superficie;
}


int  Departamento::get_precio(){
    return this->precio;
}


string  Departamento::get_ubicacion(){
    return this->ubicacion;
}


bool  Departamento::get_disponibilidad(){
    return this->disponibilidad;
}


void Departamento::set_disponibilidad(bool disponibilidad){
    this->disponibilidad = disponibilidad;
}


void Departamento::set_precio(int precio){
    this->precio = precio;
}
