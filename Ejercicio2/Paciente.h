#include <iostream>
#include "Domicilio.h"

using namespace std;

#ifndef PACIENTE_H
#define PACIENTE_H

class Paciente{
    private:
        string nombre;
        string sexo;
        string telefono;
        Domicilio domicilio;
        bool seguro;
        int num_pacientes;
        int edad;

    public:
        Paciente();
        void add_datos_paciente();
        void set_seguro(bool seguro);
        int get_edad();
        bool get_seguro();
        string get_sexo();
        string get_telefono();
        string get_nombre();
        Domicilio get_domicilio();
        
};
#endif
