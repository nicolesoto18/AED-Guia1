#include <iostream>
#include <stdio.h>
#include "Paciente.h"
#include "Domicilio.h"
#include "Secretaria.h"

using namespace std;


void menu(Secretaria secretaria){
    int opcion;
    
    cout << "\nMENU\n" << endl;
    cout << " [1] Registrar pacientes\n [2] Mostrar pacientes hospitalizados" << endl;
    cout << " [3] Buscar pacientes\n [4] Porcentaje de pacientes por sexo" << endl;
    cout << " [5] Porcetanje de pacientes con seguro\n [6] Porcentaje de pacientes por edad" << endl;
    cout << " [7] Salir" << endl;
     
    cout << "\n Ingrese su opción: ";
    cin >> opcion;
    
    system("clear");

    while(getchar() != '\n');
    
    switch(opcion){
        case 1:
            secretaria.registrar_pacientes();
            system("clear");
            menu(secretaria);
            break;

        case 2:
            secretaria.imprimir_hospitalizados();
            menu(secretaria);
            break;

        case 3:
            secretaria.imprimir_un_paciente();
            menu(secretaria);
            break;

        case 4:
            secretaria.porcentaje_sexo();
            menu(secretaria);
            break;
        
        case 5:
            secretaria.porcentaje_seguro();
            menu(secretaria);
            break;

        case 6:
            secretaria.porcentaje_edad();
            menu(secretaria);
            break;

        case 7:
            break;
    }

}
 

int main(int argc, char *argv[]){
    int num_pacientes = stoi(argv[1]);
    
    cout << "\n\tHOSPITAL ICB\n" << endl;    

    Secretaria secretaria = Secretaria(num_pacientes);
    menu(secretaria);
    
    return 0;
}
