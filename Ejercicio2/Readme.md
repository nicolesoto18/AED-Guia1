						    
                                                        	HOSPITAL ICB          
							   ---------------------

+ Empezando
    El programa cosiste en un simulador del sistema de registro de un hospital, inicialmente presenta un menú con 7 opciones. La primera opción es registrar a los pacientes, se pedira el nombre, la edad, el sexo, el teléfono, además se le preguntara si tiene seguro, después debe ingresar los datos de su domicilio, los cuales seran, la calle, el número de la calle y a que ciudad pertenece. La segunda opción le permite observar el nombre de los pacientes hospitalizados. En la tercera opción puede buscar a un paciente en específico y ver todos sus datos. La cuarta opción es el porcentaje de pacientes por sexo. La quinto elección mostrara el porcentaje de quienes tienen seguro y quienes no. En la sexta opción observara los porcentajes de pacientes dependiendo de su edad, estos se dividen en tres rangos [1] niños hasta 13 años, [2] jovenes menores de 30 y mayores de 13 años y [3] adultos mayores de 30. Finalmente la septima opción le permite al usuario cerrar el programa.


+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	[3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./programa parámetro.
		parámetro-> Número de pacientes que se registran.
        

+ Construido con
    Sistema operativo: Ubuntu 18.04.3 LTS.
    Vim: Editor de texto para escribir el código del programa. 
    C++: Lenguaje de programación imperativo orientado a objetos derivado del lenguaje C.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

+ Versiones
    Ubuntu 18.04.3 LTS.
    c++ (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0

+ Autor
    Nicole Soto.




