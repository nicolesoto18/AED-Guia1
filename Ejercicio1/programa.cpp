#include <iostream>
#include "Profesor.h"
#include "Promedio.h"

using namespace std;

int main(int argc, char *argv[]){
    int num_profesores = stoi(argv[1]);

    cout << "\n\tREGISTRO PROFESORES\n" << endl;

    // Se crea el objeto
    Promedio promedio = Promedio(num_profesores);

    // Llamar métodos
    promedio.llenar_arreglo();
    promedio.calcular_promedio();
    promedio.mayor_menor_promedio();
    promedio.profesor_joven();
    promedio.profesor_mayor();

    return 0;
}

