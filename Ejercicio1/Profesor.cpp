#include <iostream>
#include "Profesor.h"

using namespace std;

Profesor::Profesor(){
}

// Ingreso de datos
void Profesor::add_datos_profesores(){
    cout << "Ingrese el nombre del profesor: ";
    getline(cin, this->nombre);

    cout << "Ingrese la edad del profesor: ";
    cin >> this->edad;

    while(getchar() != '\n');

    cout << "\n";
}

int Profesor::get_edad(){
    return this->edad;
}

string Profesor::get_nombre(){
    return this->nombre;
}
