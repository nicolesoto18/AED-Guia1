#include <iostream>

using namespace std;

#ifndef PROFESOR_H
#define PROFESOR_H

class Profesor{
    private:
        int edad;
        string nombre;
    
    public:
        // Constructor por defecto
        Profesor();
        void add_datos_profesores();
        int get_edad();
        string get_nombre();
};
#endif

