						    
                                                        	REGISTRO PROFESORES          
							   ---------------------------
+ Empezando
    El programa cosiste en un registro para profesores, inicialmente se debe ingresar la cantidad de profesores que se inscribiran, luego se pediran los datos; nombre y edad del profesor, a través de estos datos se calculara la edad promedio y se indicara que profesores tienen una edad menor, igual o mayor al promedio, también se mostrara cual es el profesor más joven y el de más edad.


+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	[3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./programa parámetro
	     parametro -> Número de profesores que se registran.
         

+ Construido con
    Sistema operativo: Ubuntu 18.04.3 LTS.
    Vim: Editor de texto para escribir el código del programa. 
    C++: Lenguaje de programación imperativo orientado a objetos derivado del lenguaje C.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

+ Versiones
    Ubuntu 18.04.3 LTS.
    c++ (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0

+ Autor
    Nicole Soto.




